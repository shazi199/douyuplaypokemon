package us.sunrisemorning.douyuplaypokemon;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class RobotControl extends Thread {

	private Queue<String> queue = new Queue<String>();
	private final Robot robot;

	public RobotControl() throws AWTException {
		robot = new Robot();
	}

	public synchronized void push(String message) {
		if (queue.size() < 10) {
			queue.put(message);
		}
	}

	@Override
	public void run() {
		while (true) {
			String message = queue.pull();
			switch (message) {
			case "up":
				pressKey(KeyEvent.VK_I);
				break;
			case "down":
				pressKey(KeyEvent.VK_K);
				break;
			case "left":
				pressKey(KeyEvent.VK_J);
				break;
			case "right":
				pressKey(KeyEvent.VK_L);
				break;
			case "a":
				pressKey(KeyEvent.VK_Z);
				break;
			case "b":
				pressKey(KeyEvent.VK_X);
				break;
			case "select":
				pressKey(KeyEvent.VK_C);
				break;
			case "start":
				pressKey(KeyEvent.VK_V);
				break;
			}
		}
	}

	private void pressKey(int keyCode) {
		robot.keyPress(keyCode);
		robot.delay(100);
		robot.keyRelease(keyCode);
		robot.delay(100);
	}
}
