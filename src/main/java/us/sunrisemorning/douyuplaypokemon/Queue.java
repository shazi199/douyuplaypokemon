package us.sunrisemorning.douyuplaypokemon;

import java.util.Vector;

public class Queue<E> {
	private Vector<E> vector = new Vector<E>();

	public synchronized void put(E object) {
		vector.addElement(object);
		notify();
	}

	public synchronized E pull() {
		while (isEmpty())
			try {
				wait();
			} catch (InterruptedException ex) {
			}
		return get();
	}

	public synchronized E get() {
		E object = peek();
		if (object != null)
			vector.removeElementAt(0);
		return object;
	}

	public E peek() {
		if (isEmpty())
			return null;
		return vector.elementAt(0);
	}

	public boolean isEmpty() {
		return vector.isEmpty();
	}

	public int size() {
		return vector.size();
	}
}
