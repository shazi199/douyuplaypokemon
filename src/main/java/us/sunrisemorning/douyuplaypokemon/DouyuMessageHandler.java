package us.sunrisemorning.douyuplaypokemon;

import java.awt.AWTException;
import java.util.ArrayList;
import java.util.List;

import us.sunrisemorning.douyuchat.Message;
import us.sunrisemorning.douyuchat.MessageHandler;

public class DouyuMessageHandler implements MessageHandler {

	private final List<String> orders = new ArrayList<String>();
	private final RobotControl control;

	public DouyuMessageHandler() throws AWTException {
		orders.add("up");
		orders.add("down");
		orders.add("left");
		orders.add("right");
		orders.add("a");
		orders.add("b");
		orders.add("select");
		orders.add("start");

		control = new RobotControl();
		control.start();
	}

	@Override
	public void handle(Message message) {
		switch (message.getType()) {
		case "chatmsg":
			String txt = message.get("txt").toLowerCase();
			if (orders.contains(txt)) {
				control.push(txt);
			}
			break;
		}
	}
}
